# README #

A simple app to track a standard list of people that attend your stand up. This app also includes functionality to easily remove people who aren't attending, add guests that are attending, and "mark" who has already spoken.
Most useful for remote standups.

* Version
v1.0.0

### How do I get set up? ###

* Navigate to where you want to save the project
* Clone the project
* Run bower install. **Note:** this installs the dependencies one directory up from where you clone the project.
* Replace the people in *persons.json* with the standard list of people whom attend your standup. **firstName and lastName are required for each person**.
* Spin up a SimpleHTTPServer from the **root directory** of the cloned project
* Navigate to the index file inside the **/standuptracker/** directory in a web browser

Alternatively, you can edit your bash profile to automate the task of launching the project:

```
#!bash

standup()
{
	cd >> path_to_where_you_download_it << ;
	python -m SimpleHTTPServer 7000 &;
	open -a "Google Chrome" 'http://localhost:7000/standuptracker';
	open -a "Google Chrome" 'http://infusionsoft.webex.com';
}

killst() {
	ps aux | grep SimpleHTTPServer | awk '{ print $2 }' | xargs kill -9;
}
```


Thanks for looking!